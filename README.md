# confront.tech/ng-util

## ng-util

Common utilities I use across Angular projects.

### rxjs

- subscription handling

### web-worker

- observable web-worker wrapper

### debugging

- generate sounds when DOM is manipulated (to identify components that re-render when they shouldn't)
