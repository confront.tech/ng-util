import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import * as path from 'path';
import { appOptions, workspaceOptions } from '../../testing/schematics';

// https://blog.angular.io/angular-schematics-unit-testing-3a0a9aaab186
export const collectionPath = path.join(__dirname, '../collection.json');

describe('dependencies', () => {
  let appTree: UnitTestTree;
  let runner: SchematicTestRunner;

  beforeEach(async () => {
    runner = new SchematicTestRunner('schematics', collectionPath);
    appTree = await runner
      .runExternalSchematicAsync('@schematics/angular', 'workspace', workspaceOptions)
      .toPromise();
  });

  ['application', 'library'].forEach((type) => {
    describe(`type: ${type}`, () => {
      beforeEach(async () => {
        appTree = await runner
          .runExternalSchematicAsync('@schematics/angular', type, appOptions, appTree)
          .toPromise();
      });

      it('works', async () => {
        const tree = await runner.runSchematicAsync('dependencies', {}, appTree).toPromise();
        const pkg = JSON.parse(tree.get('/package.json')?.content.toString()!);
        expect(pkg.husky.hooks).toEqual({ 'pre-commit': 'lint-staged' });
        expect(pkg['lint-staged']).toEqual({
          '{projects, src}/**/*.{ts, js, css, scss, html, json}': ['prettier --write'],
        });
        expect(pkg.scripts.lint).toEqual('ng lint --fix');
        expect(pkg.scripts.format).toEqual('prettier --write .');

        expect(JSON.parse(tree.get('/.prettierrc')?.content.toString()!)).toEqual({
          singleQuote: true,
          printWidth: 100,
          trailingComma: 'es5',
        });
      });
    });
  });
});
