import { Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { getAngularVersion } from '../util/angular';
import {
  addPackageJsonDependency,
  addPropertyToPackageJson,
  FrameworkConfig,
  FrameworkMap,
  NodeDependencyType,
} from '../util/dependencies';

export function dependencies(_options: any): Rule {
  return (tree: Tree, context: SchematicContext) => {
    addDependencies(tree, context);
  };
}

function addDependencies(tree: Tree, context: SchematicContext) {
  const angularVersion = getAngularVersion(tree);

  const dependencies: FrameworkMap = {
    ['@angular/cdk']: `~${angularVersion}.0.0`,
  };
  const devDependencies: FrameworkMap = {
    ['prettier']: '^2.0.0',
    ['husky']: '^4.2.0',
    ['lint-staged']: '^10.2.0',
  };
  const dependenciesConfig: FrameworkConfig = {
    ['husky']: {
      hooks: {
        'pre-commit': 'lint-staged',
      },
    },
    ['lint-staged']: {
      '{projects, src}/**/*.{ts, js, css, scss, html, json}': ['prettier --write'],
    },
  };
  const scriptsConfig: FrameworkMap = {
    ['lint']: 'ng lint --fix',
    ['format']: 'prettier --write .',
  };

  Object.entries(dependencies).forEach(([name, value]) =>
    addPackageJsonDependency(tree, {
      name,
      version: value,
      type: NodeDependencyType.Default,
    })
  );

  Object.entries(devDependencies).forEach(([name, value]) =>
    addPackageJsonDependency(tree, {
      name,
      version: value,
      type: NodeDependencyType.Dev,
    })
  );

  Object.entries(dependenciesConfig).forEach(([key, value]) =>
    addPropertyToPackageJson(tree, context, key, value)
  );

  Object.entries(scriptsConfig).forEach(([key, value]) =>
    addPropertyToPackageJson(tree, context, 'scripts', {
      [key]: value,
    })
  );

  if (!tree.exists('/.prettierrc')) {
    tree.create(
      '/.prettierrc',
      JSON.stringify(
        {
          singleQuote: true,
          printWidth: 100,
          trailingComma: 'es5',
        },
        null,
        2
      )
    );
  }

  context.addTask(new NodePackageInstallTask());
}
