import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import * as path from 'path';
import { appOptions, workspaceOptions } from '../../testing/schematics';

// https://blog.angular.io/angular-schematics-unit-testing-3a0a9aaab186

const collectionPath = path.join(__dirname, '../collection.json');

describe('jest', () => {
  let appTree: UnitTestTree;
  let runner: SchematicTestRunner;

  beforeEach(async () => {
    runner = new SchematicTestRunner('schematics', collectionPath);
    appTree = await runner
      .runExternalSchematicAsync('@schematics/angular', 'workspace', workspaceOptions)
      .toPromise();
  });

  ['application', 'library'].forEach((type) => {
    describe(`type: ${type}`, () => {
      beforeEach(async () => {
        appTree = await runner
          .runExternalSchematicAsync('@schematics/angular', type, appOptions, appTree)
          .toPromise();
      });

      it('works', async () => {
        const tree = await runner.runSchematicAsync('jest', {}, appTree).toPromise();
        const pkg = JSON.parse(tree.get('/package.json')?.content.toString()!);
        const ang = JSON.parse(tree.get('/angular.json')?.content.toString()!);

        expect(pkg.devDependencies.jest).toBeTruthy();
        expect(pkg.devDependencies['jest-preset-angular']).toBeTruthy();
        expect(ang.projects[appOptions.name].architect.test.builder).toBe(
          '@angular-builders/jest:run'
        );
      });
    });
  });
});
