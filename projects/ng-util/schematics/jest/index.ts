import { WorkspaceTool } from '@angular-devkit/core/src/experimental/workspace';
import { chain, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import { concat, Observable, of } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { TsConfigSchema } from '../model';
import { getAngularVersion, getWorkspace, overwriteWorkspace } from '../util/angular';
import {
  addPackageJsonDependency,
  NodeDependencyType,
  removePackageJsonDependency,
} from '../util/dependencies';
import { parseJsonAtPath } from '../util/json';
import { getLatestNodeVersion, NodePackage, safeFileDelete } from '../util/util';
import { JestOptions } from './model';
import { getWorkspaceConfig } from './util';

export function jest(options: JestOptions): Rule {
  return (tree: Tree, context: SchematicContext) => {
    options = { ...options, __version__: getAngularVersion(tree) };

    return chain([
      updateDependencies(),
      removeFiles(),
      addJestConfigFile(),
      configureTsConfig(options),
      configureAngularJsonTestBuilder(options),
    ])(tree, context);
  };
}

function updateDependencies(): Rule {
  return (tree: Tree, context: SchematicContext): Observable<Tree> => {
    context.logger.debug('Updating dependencies...');
    context.addTask(new NodePackageInstallTask());

    const removeDependencies = of(
      'karma',
      'karma-jasmine',
      'karma-jasmine-html-reporter',
      'karma-chrome-launcher',
      'karma-coverage-istanbul-reporter'
    ).pipe(
      map((packageName: string) => {
        context.logger.debug(`Removing ${packageName} dependency`);

        removePackageJsonDependency(tree, {
          type: NodeDependencyType.Dev,
          name: packageName,
        });

        return tree;
      })
    );

    const addDependencies = of(
      '@angular-builders/jest',
      '@types/jest',
      'jest',
      'jest-preset-angular'
    ).pipe(
      concatMap((packageName: string) => getLatestNodeVersion(packageName)),
      map((packageFromRegistry: NodePackage) => {
        const { name, version } = packageFromRegistry;
        context.logger.debug(`Adding ${name}:${version} to ${NodeDependencyType.Dev}`);

        addPackageJsonDependency(tree, {
          type: NodeDependencyType.Dev,
          name,
          version,
        });

        return tree;
      })
    );

    return concat(removeDependencies, addDependencies);
  };
}

function addJestConfigFile(): Rule {
  return (tree: Tree, context: SchematicContext) => {
    const filePath = 'jest.config.js';
    context.logger.debug(`Adding ${filePath}`);
    tree.create(`/${filePath}`, '');
    return tree;
  };
}
function removeFiles(): Rule {
  return (tree: Tree, context: SchematicContext) => {
    const deleteFiles = [
      './src/karma.conf.js',
      './karma.conf.js',
      './src/test.ts',

      // unable to overwrite these with the url() approach.
      './jest.config.js',
      './src/setup-jest.ts',
      './src/test-config.helper.ts',
    ];

    deleteFiles.forEach((filePath) => {
      context.logger.debug(`removing ${filePath}`);

      safeFileDelete(tree, filePath);
    });

    return tree;
  };
}

function configureTsConfig(options: JestOptions): Rule {
  return (tree: Tree) => {
    const { projectProps } = getWorkspaceConfig(tree, options);
    const tsConfigPath = projectProps.architect.test.options.tsConfig;
    const workplaceTsConfig = parseJsonAtPath(tree, tsConfigPath);

    let tsConfigContent: TsConfigSchema;

    if (workplaceTsConfig && workplaceTsConfig.value) {
      tsConfigContent = workplaceTsConfig.value;
    } else {
      return tree;
    }

    tsConfigContent.compilerOptions = Object.assign(tsConfigContent.compilerOptions, {
      emitDecoratorMetadata: true,
      esModuleInterop: true,
      types: ['jest', 'node'],
    });
    tsConfigContent.files = tsConfigContent.files.filter(
      (file: string) => !['test.ts', 'src/test.ts'].includes(file)
    );

    return tree.overwrite(tsConfigPath, JSON.stringify(tsConfigContent, null, 2) + '\n');
  };
}

function configureAngularJsonTestBuilder(options: JestOptions): Rule {
  return (tree: Tree) => {
    const workspace = getWorkspace(tree);
    const { projectName } = getWorkspaceConfig(tree, options);

    (workspace.projects[projectName].architect as WorkspaceTool).test = {
      builder: '@angular-builders/jest:run',
    };

    overwriteWorkspace(tree, workspace);
  };
}
