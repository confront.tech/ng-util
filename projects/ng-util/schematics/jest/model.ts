export interface JestOptions {
  updateTests?: boolean;
  project?: string;
  config?: 'file' | 'packagejson' | string;
  overwrite?: boolean;
  __version__?: number;
}
