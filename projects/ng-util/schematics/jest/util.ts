import { join, Path } from '@angular-devkit/core';
import { SchematicsException } from '@angular-devkit/schematics';
import { Tree } from '@angular-devkit/schematics/src/tree/interface';
import { getWorkspace, getWorkspacePath } from '../util/angular';
import { JestOptions } from './model';

export function getSourcePath(tree: Tree, options: any): string {
  const workspace = getWorkspace(tree);

  if (!options.project) {
    throw new SchematicsException('Option "project" is required.');
  }

  const project = workspace.projects[options.project];

  if (project.projectType !== 'application') {
    throw new SchematicsException(
      `AddJest requires a project type of "application".`
    );
  }

  // const assetPath = join(project.root as Path, 'src', 'assets');
  const sourcePath = join(project.root as Path, 'src');

  return sourcePath;
}

export function getWorkspaceConfig(
  tree: Tree,
  options: JestOptions
): {
  projectProps: any;
  workspacePath: string;
  workspace: any;
  projectName: any;
} {
  const workspace = getWorkspace(tree);
  const workspacePath = getWorkspacePath(tree);
  let projectName;
  let projectProps;

  projectName = options.project || workspace.defaultProject || '';
  projectProps = workspace.projects[projectName] || (workspace as any).apps[0];

  return { projectProps, workspacePath, workspace, projectName };
}
