import { experimental } from '@angular-devkit/core';

export enum pkgJson {
  Path = '/package.json',
}

export type WorkspaceSchema = experimental.workspace.WorkspaceSchema;

export * from './ts-config-schema';

export interface NpmRegistryPackage {
  name: string;
  version: string;
}
