import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Schema as ApplicationOptions } from '@schematics/angular/application/schema';
import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';
import * as path from 'path';
import { NgAddOptionsSchema } from '.';

const collectionPath = path.join(__dirname, '../collection.json');
const workspaceOptions: WorkspaceOptions = {
  name: 'workspace',
  newProjectRoot: 'projects',
  version: '6.0.0',
};

const appOptions: ApplicationOptions = {
  name: 'bar',
  inlineStyle: false,
  inlineTemplate: false,
  routing: false,
  skipTests: false,
  skipPackageJson: false,
};

describe('ng-add', () => {
  let appTree: UnitTestTree;
  let runner: SchematicTestRunner;

  beforeEach(async () => {
    runner = new SchematicTestRunner('schematics', collectionPath);
    appTree = await runner
      .runExternalSchematicAsync('@schematics/angular', 'workspace', workspaceOptions)
      .toPromise();
    appTree = await runner
      .runExternalSchematicAsync('@schematics/angular', 'application', appOptions, appTree)
      .toPromise();
  });

  // it('always runs dependencies schematic', () => {});

  [false, true].forEach((jest) => {
    it(`asks to install jest and ${jest ? 'installs' : `doesn't install`}`, async () => {
      const tree = await runner
        .runSchematicAsync('ng-add', { jest } as NgAddOptionsSchema, appTree)
        .toPromise();

      // console.log(tree.get('/package.json')?.content.toString()!);
      const pkg = JSON.parse(tree.get('/package.json')?.content.toString()!);

      if (jest) {
        expect(pkg.devDependencies.jest).toBeTruthy();
      } else {
        expect(pkg.devDependencies.jest).not.toBeTruthy();
      }
    });
  });
});
