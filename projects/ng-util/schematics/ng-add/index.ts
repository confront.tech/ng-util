import { chain, noop, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { dependencies } from '../dependencies';
import { jest } from '../jest';

export interface NgAddOptionsSchema {
  jest?: boolean;
}

export function ngAdd(options: NgAddOptionsSchema): Rule {
  return (tree: Tree, context: SchematicContext) => {
    return chain([dependencies({}), options && options.jest ? jest({}) : noop()])(tree, context);
  };
}
