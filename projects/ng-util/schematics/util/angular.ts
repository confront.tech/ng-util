import { JsonParseMode, parseJson } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import { WorkspaceSchema } from '../model';
import { getPackageJsonDependency } from './dependencies';

export function getAngularVersion(tree: Tree): number {
  const packageNode = getPackageJsonDependency(tree, '@angular/core');

  const version = (packageNode && packageNode.version) || '.';

  return +version.replace('~', '').replace('^', '').split('.')[0] || 0;
}

export function getWorkspacePath(tree: Tree): string {
  const possibleFiles = ['/angular.json', '/.angular.json'];
  const path = possibleFiles.filter((path) => tree.exists(path))[0];

  return path;
}

export function getWorkspace(tree: Tree): WorkspaceSchema {
  const path = getWorkspacePath(tree);
  const configBuffer = tree.read(path);
  if (configBuffer === null) {
    throw new SchematicsException(`Could not find (${path})`);
  }
  const content = configBuffer.toString();

  return (parseJson(content, JsonParseMode.Loose) as {}) as WorkspaceSchema;
}

export function overwriteWorkspace(tree: Tree, workspace: WorkspaceSchema) {
  const path = getWorkspacePath(tree);
  tree.overwrite(path, JSON.stringify(workspace, null, 2) + '\n');
}
