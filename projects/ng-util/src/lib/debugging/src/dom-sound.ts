export type DOMSoundRelease = () => void;

/**
 * Will play sounds on DOM manipulations.
 *
 * @example
 * app.component.ts
 *
 * private readonly domSoundRelease = startDOMSound();
 * ngOnDestroy() {
 *  this.domSoundRelease();
 * }
 */
export const startDOMSound = (): DOMSoundRelease => {
  const ctxAudio: AudioContext = new ((window as any).AudioContext ||
    (window as any).webkitAudioContext)();
  const observer = new MutationObserver((mutations) => {
    const oscillator = ctxAudio.createOscillator();

    oscillator.connect(ctxAudio.destination);
    oscillator.type = 'sine';
    oscillator.frequency.setValueAtTime(
      Math.log(mutations.length + 5) * 800,
      ctxAudio.currentTime
    );

    oscillator.start();
    oscillator.stop(ctxAudio.currentTime + 0.01);
  });

  observer.observe(document, {
    attributes: true,
    childList: true,
    subtree: true,
    characterData: true,
  });

  return () => {
    observer.disconnect();
    ctxAudio.close();
  };
};
