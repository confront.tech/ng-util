import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

/**
 * Returns an `Observable` which wraps the `Worker` from `workerGetter`.
 * The `Worker` in question is supposed to make single run i.e. only one `postMessage` from inside of the `Worker` gets evaluated.
 * @param workerGetter callback that returns the `Worker`
 *
 * @example
 * .pipe(
 *   switchMap(workerSingleRun$(() => new Worker('./my-worker', {type: module})))
 * )
 *
 * The `workerGetter`s `Worker` has to implement the following functionality:
 * @example
 * addEventListener('message', ({data}) => {
 *  try {
 *    postMessage({data: my_worker_function(data), error: null});
 *  } catch(ex) {
 *    postMessage({data: null, error: ex || 'error});
 *  }
 * })
 */

export const workerSingleRun$ = <T, R>(workerGetter: () => Worker) => (
  value: T
) =>
  new Observable<R>((subscriber) => {
    let worker: Worker | null = null;
    try {
      worker = workerGetter();
    } catch (ex) {
      subscriber.error(ex);
    }

    if (worker) {
      worker.onerror = subscriber.error;
      worker.onmessage = (data) => {
        if (data.data.error) {
          subscriber.error(data.data.error);
        } else {
          subscriber.next(data.data.data as R);
          subscriber.complete();
        }
      };
      worker.postMessage(value);
    }

    return () => {
      if (worker) {
        worker.terminate();
        worker = null;
      }
    };
  });

export const transformWorker$ = <T, R>(
  transformCallback: (value: T) => R,
  workerGetter: () => Worker
) =>
  typeof Worker !== 'undefined'
    ? workerSingleRun$<T, R>(workerGetter)
    : (value: T) => of(transformCallback(value));

@Injectable({
  providedIn: 'root',
})
export class WorkerService {
  transform = transformWorker$;
}
